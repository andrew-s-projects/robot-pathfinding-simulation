function [modes, times] = p2pReach(initialState, targetPosition)
    u  = [targetPosition(1) - initialState(1), targetPosition(2) - initialState(2)] 
     % compute cross product between heading and position vector to determine turn direction
    delta_theta = computeDeltaTheta(initialState, targetPosition, initialState(3))
    if delta_theta == 0
        % heading direction will directly hit target - go straight
        modes = ['S']
        
        % calculate total time to reach dest.
        times = [norm(u) / (pi * 0.2)]
        
    elseif delta_theta > 0 
        % left then straight
        
        % calculate time spent in left turn (time/velocity)
        left_time = delta_theta / (pi * 0.2)
        
        % compute discretization of left turn 
        [xL, yL] = disc('L', initialState, delta_theta)
        
        % then time spent in S is norm of targetPosition and disc vector
        disc_vector = [targetPosition(1)-xL, targetPosition(2)- yL]
        time_straight = norm(disc_vector) / (pi * 0.2)
        modes =['L', 'S']
        times = [abs(left_time), time_straight]
    else
         % right then straight
         
         % same procedure as previous
         
        right_time = delta_theta / (pi * 0.2)
        % compute discretization of right turn
        [xL, yL] = disc('R', initialState, delta_theta)
        disc_vector = [targetPosition(1)-xL, targetPosition(2)- yL]
        time_straight = norm(disc_vector) / (pi * 0.2)
        modes = ['R', 'S']
        times = [abs(right_time), time_straight]
        
        
    end
end
% computes discretization of turn (next state)
function [xL, yL] = disc(turn, currentState, theta)
    L = 1
    R = 0.2
    if turn == "R"
        xL = currentState(1) + L/2 * (sin(theta) - sin(theta - R/L * pi))
        yL = currentState(2) + L/2 * (cos(theta - R/L * pi) - cos(theta))
    else
        xL = currentState(1) + L/2 * (sin(theta + R/L * pi) - sin(theta))
        yL = currentState(2) + L/2 * (cos(theta) - cos(theta + R/L * pi))
    end