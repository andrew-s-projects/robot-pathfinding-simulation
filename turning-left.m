function [finalState, states] = turningLeft(currentState, DeltaT)

    [t, states] = ode45(@turning_left_dynamics, [0 DeltaT], currentState);
    finalState = states(end,:)';
end

function dxdt = turning_left_dynamics(t, state)
    wR= pi;
    R = 0.2;
    L = 1;
    x = state(1);
    y = state(2);
    theta = state(3);
    
    dxdt(1, 1) = R/2 * wR * cos(theta);
    dxdt(2, 1) = R/2 * wR * sin(theta);
    dxdt(3, 1) = R/L * wR;
end