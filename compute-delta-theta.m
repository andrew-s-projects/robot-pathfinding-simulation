function DeltaTheta = computeDeltaTheta(initialPosition, targetPosition, theta)
     L =  1
     
     % compute position vector and its magnitude
     u = [targetPosition(1)-initialPosition(1), targetPosition(2)-initialPosition(2)]
     v = [cos(theta), sin(theta)]
     
     % compute cross product between heading and position vector - the sign
     % reveals turn direction
     uv_cross = (v(1) * u(2)) - (v(2)* u(1))     
     
     % check range of uv cross product
     if uv_cross >= 0 - 1e-12 && uv_cross <= 0 + 1e-12
         uv_cross = 0;
     end
     
     if uv_cross >= 0
         % left turn
         
         % compute center of turn 
         xc = initialPosition(1) + L * cos(theta + pi/2)
         yc = initialPosition(2) + L * sin(theta + pi/2)
         a = [(targetPosition(1) - xc), (targetPosition(2) - yc)]
         b = [(initialPosition(1) - xc), (initialPosition(2) - yc)]
         
         % compute alpha as dot product of a/b
         a_leng = norm(a) 
         b_leng = norm(b)
         beta = acos(L / a_leng)   
         a_dot = (a(1) * b(1) + a(2) * b(2)) / (a_leng * b_leng)
         alpha = acos(a_dot)
         
         % check range of alpha using cross product of a/b
         ab_cross = ((a(1) * b(2) - a(2) * b(1))) / (a_leng * b_leng)
         if ab_cross > 0
             % big turn
             DeltaTheta = 2* pi - alpha - beta            
         else
            DeltaTheta = alpha - beta
         end
        
     else
         % right turn 
         
         % compute center of turn 
         xc = initialPosition(1) + L/2 * cos(theta - pi/2)
         yc = initialPosition(2) + L/2 * sin(theta - pi/2)
         a = [(targetPosition(1) - xc), (targetPosition(2) - yc)]
         b = [(initialPosition(1) - xc), (initialPosition(2) - yc)]
         
         % compute alpha 
         a_leng = norm(a)
         b_leng = norm(b)
         beta = acos(L/2 / a_leng)
         a_dot = (a(1) * b(1) + a(2) * b(2)) / (a_leng * b_leng)
         
         % check range of a/b
         ab_cross = ((a(1) * b(2) - a(2) * b(1))) / (a_leng * b_leng)
         alpha = acos(a_dot)
       
         if ab_cross > 0
             % big turn
             DeltaTheta = -(alpha - beta)
         else
             DeltaTheta = -(2* pi - alpha - beta)
         end
     end
     
end