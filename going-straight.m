function [finalState, states] = goingStraight(currentState, DeltaT)

    [t, states] = ode45(@going_straight_dynamics, [0 DeltaT], currentState);
    finalState = states(end,:)'; 
end

function dxdt = going_straight_dynamics(t, state)
    w = pi;
    R = 0.2;
    L = 1;
    x = state(1);
    y = state(2);
    theta = state(3);
    
    dxdt(1, 1) = R * w * cos(theta);
    dxdt(2, 1) = R * w * sin(theta);
    dxdt(3, 1) = 0;
end
