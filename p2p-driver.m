function p2p_driver

    % for demonstration
   
    x = input("Enter Example number to Simulate.... 1,2,3,4,5\n")
    switch x
        case 1
            initial_state = [0,0, 3*pi/4];
            target_position = [-3,3];
        case 2
            initial_state = [-1,2, pi];
            target_position = [2,-3];
        case 3
            initial_state = [-2,-1, 5*pi/4];
            target_position = [2,3];
        case 4
            initial_state = [-5,-1, pi/4];
            target_position = [-4,-1];
        case 5
            initial_state = [0,0, pi/2];
            target_position =[1,-2];
        otherwise
             disp("Incorrect use: example number must be between 1-5")
             return;
    end

    [modes, times] = p2pReach(initial_state, target_position);
    steer_two_wheeled_robot_offline(initial_state, modes, times)

end