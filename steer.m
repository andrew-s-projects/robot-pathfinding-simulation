function steer_two_wheeled_robot_offline(initialState, modes, times)

    currentState = initialState;
    reachable = [];
    N = length(modes);
    
    for i = 1:N
        
      duration = times(i);
      
      if modes(i) == "S"
      
            [finalState, states] = goingStraight(currentState, duration);
            reachable = [reachable; states];
            currentState = finalState;
            
      elseif modes(i) == "R"
         
            [finalState, statesRight] = turningRight(currentState, duration);
            reachable = [reachable; statesRight];
            currentState = finalState;
      else
          [finalState, statesLeft] = turningLeft(currentState, duration);
          reachable = [reachable; statesLeft];
          currentState = finalState;   
      end
      plot(reachable(:,1), reachable(:,2), 'color','b');
      axis equal;
    end
end